# Conditional CICD

This project aims to showcase how to use Gitlab CI/CD to conditionally build and deploy modules in a multi-module maven 
project.

## Motivation

Gitlab CI/CD provides `rules:changes:compare_to` and `rules:changes:paths` to control whether a job should be added to 
the pipeline depending on changes specified in `rules:changes:paths`.

This does work for merge request pipelines, where specifying the `rules:changes:compare_to` allows us to compare between
the latest commit (if branch is provided) of the specified branch and the merge request source branch.

However, this does not work as one would expect in branch pipelines because the default behaviour is to compare `HEAD` 
and `HEAD^` (referring to the first parent commit) 
as seen [here](https://docs.gitlab.com/ee/ci/jobs/job_control.html#jobs-or-pipelines-run-unexpectedly-when-using-changes)

Take for instance, you are merging 2 commits into the default branch where the first commit is a change to module A and 
the second commit is a change to module B.
Only changes in the second commit will be detected in branch pipelines (due to merge commit) and thus **only** module B 
will be built and deployed.
Module A which was changed in the first commit will **not** be built and deployed, which is not what we want.

## Additional issues of note

1. `rules:changes:compare_to` does not support variable expansion, so an input like `CI_COMMIT_BEFORE_SHA` cannot be 
used.
2. There is no such thing as adding a job only if a specified job is added.
3. Rules are evaluated when the pipeline is created, so we cannot use rules:if to check a variable that indicates a job 
has been added.
4. When a trigger job runs, the child pipeline must be created, and it cannot be empty, otherwise the trigger job will 
fail.
5. trigger and script cannot be present in the same job.

## Solution

### 1. Change Detection

Templates used are present [here](https://gitlab.com/personal6855003/template)

Since Gitlab's `rules:changes:compare_to` does not work as we want, we will use the 
[git diff](https://git-scm.com/docs/git-diff) command to check for changes in the child module.

In particular, the `--quiet` option implies `--exit-code` which will configure `git diff` to return `1` if there are 
differences and `0` if there are none.

In the [maven-multi-module-change-trigger-template](https://gitlab.com/personal6855003/template/-/blob/master/maven-multi-module-change-trigger-template.yml), we
used this bash script in the change-detection job:

```bash
if [[ $CI_PIPELINE_SOURCE == 'merge_request_event' ]]; then
    git fetch origin $CI_MERGE_REQUEST_SOURCE_BRANCH_NAME
    git fetch origin $CI_MERGE_REQUEST_TARGET_BRANCH_NAME
    if git diff --quiet origin/$CI_MERGE_REQUEST_SOURCE_BRANCH_NAME origin/$CI_MERGE_REQUEST_TARGET_BRANCH_NAME $[[ inputs.module-folder ]]/**/*; then
        echo "CHANGE=no" >> change.env
    else
        echo "CHANGE=yes" >> change.env
    fi
else
    if [[ $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH ]]; then
        if git diff --quiet $CI_COMMIT_BEFORE_SHA $CI_COMMIT_SHA $[[ inputs.module-folder ]]/**/*; then
            echo "CHANGE=no" >> change.env
        else
            echo "CHANGE=yes" >> change.env
        fi
    else
        git fetch origin $CI_DEFAULT_BRANCH
        if git diff --quiet $CI_COMMIT_SHA origin/$CI_DEFAULT_BRANCH $[[ inputs.module-folder ]]/**/*; then
            echo "CHANGE=no" >> change.env
        else
            echo "CHANGE=yes" >> change.env
        fi
    fi
fi
```

The script first checks if the pipeline is a merge request pipeline or a branch pipeline using the variable 
`CI_PIPELINE_SOURCE`.

For merge request pipelines, we will compare `CI_MERGE_REQUEST_SOURCE_BRANCH_NAME` (the source branch of the merge 
request) and `CI_MERGE_REQUEST_TARGET_BRANCH_NAME` (the target branch of the merge request) to check for changes made in
the merge request. To do so, we need to fetch the source and target branch from the remote repository.

We cannot use commit SHA like `CI_MERGE_REQUEST_SOURCE_COMMIT_SHA` and `CI_MERGE_REQUEST_TARGET_COMMIT_SHA` because they
are empty in merge request pipelines. [See](https://docs.gitlab.com/ee/ci/variables/predefined_variables.html#predefined-variables-for-merge-request-pipelines)

For branch pipelines, there are two cases. 

The first case is the default branch pipeline. For this, we will compare `CI_COMMIT_BEFORE_SHA` (previous latest commit present on the branch) and
`CI_COMMIT_SHA` (the commit revision the project is built for) to check for changes across all commits added to the
branch. No fetching is required here since the commits are already present in the local repository.

The second case is the non-default branch pipeline. For this, we will compare `CI_COMMIT_SHA` (the commit revision the
project is built for) and `CI_DEFAULT_BRANCH` (the default branch of the project) to check for changes across all
commits added in the non-default branch from the default branch. To do so, we only need to fetch the default branch 
from the remote repository since the commits in the non-default branch are already present in the local repository.

The script then writes the result of the comparison to the variable `CHANGE` in the file `change.env`, which will be 
output as a dotenv report artifact.

### 2. Triggering child pipeline

Also in the [maven-multi-module-change-trigger-template](https://gitlab.com/personal6855003/template/-/blob/master/maven-multi-module-change-trigger-template.yml), we
have the trigger job, which is separate from the change-detection job.

This is because script and trigger cannot be present in the same job.

The trigger job will specify in `needs` the change-detection job that corresponds to the module it is triggering the 
child pipeline for.
This ensures we get the dotenv report artifact from the correct change-detection job. (Otherwise, we might see changes 
when there are none)

The trigger job then takes the value of variable `CHANGE` from the dotenv report artifact and passes it as a trigger 
variable `TRIGGER` to the child pipeline. [See](https://docs.gitlab.com/ee/ci/pipelines/downstream_pipelines.html#pass-yaml-defined-cicd-variables)

The trigger job assumes the child module has a .gitlab-ci.yml and uses it to trigger a child pipeline.

### 3. Child pipeline

Because the child pipeline is created by the trigger job, rules of jobs in the child pipeline is freshly evaluated.

This means that we can effectively use the trigger variable `TRIGGER` (passed down by the trigger job) in `rules:if`. We
can now conditionally include jobs in the child pipeline depending on the value of `TRIGGER`.

Since trigger variables take higher precedence than job variables, we can also define jobs with the `TRIGGER` job 
variable to use their values as default in cases where the trigger variable `TRIGGER` is not present. [See](https://docs.gitlab.com/ee/ci/variables/index.html#cicd-variable-precedence)

## Example

### Case 1: Default branch pipeline with changes in firstproject module and secondproject module across different commits

In this [pipeline](https://gitlab.com/personal6855003/conditionalcicd/-/pipelines/1143977080), we can see that the build
and deploy jobs for both the firstproject module and secondproject module are run.

This pipeline is triggered by a push of a total of 3 commits, where the first commit `58bd0f0c` is an update to the 
`README.md`, the second commit `e6409756` is a change to the `firstproject` module and the third commit `e995526b` is a 
change to the `secondproject` module.

![firstproject child pipeline](assets/img_4.png)

![secondproject child pipeline](assets/img_5.png)

A closer look at the log output of the change detection jobs shows that `CHANGE=yes` appears for both the
`change-detection-firstproject-job` and the `change-detection-secondproject-job`.

[change-detection-firstproject-job](https://gitlab.com/personal6855003/conditionalcicd/-/jobs/5965051756)
![change-detection-firstproject-job log](assets/img_6.png)

[change-detection-secondproject-job](https://gitlab.com/personal6855003/conditionalcicd/-/jobs/5965051758)
![change-detection-secondproject-job log](assets/img_7.png)

### Case 2: Default branch pipeline with only changes in firstproject module

In this [pipeline](https://gitlab.com/personal6855003/conditionalcicd/-/pipelines/1143961073), we can see that only the
build and deploy jobs for the firstproject module are run, whereas the build and deploy jobs for the secondproject are
not.

![firstproject child pipeline](assets/img.png)

![secondproject child pipeline](assets/img_1.png)

A closer look at the log output of the change detection jobs shows that `CHANGE=yes` only appears for the 
`change-detection-firstproject-job`.

[change-detection-firstproject-job](https://gitlab.com/personal6855003/conditionalcicd/-/jobs/5964963967)
![change-detection-firstproject-job log](assets/img_2.png)

[change-detection-secondproject-job](https://gitlab.com/personal6855003/conditionalcicd/-/jobs/5964963968)
![change-detection-secondproject-job log](assets/img_3.png)

### Case 3: Non-default branch pipeline with changes in only firstproject module

This [pipeline](https://gitlab.com/personal6855003/conditionalcicd/-/pipelines/1144677640) is run for a non-default
branch `chenenliu/change-firstproject` which is created from the default branch `main` with a single commit making
changes to the `firstproject` module.

No deploy jobs are run because the deploy jobs are configured to only run for default branch pipelines.

We see that only the build job for the firstproject module is run, whereas the build job for the secondproject module
is not.

### Case 4: Merge request pipeline with changes in only firstproject module

This [merge request pipeline](https://gitlab.com/personal6855003/conditionalcicd/-/pipelines/1144733559) is run for a
merge request from a non-default branch `chenenliu/change-firstproject-mr` to the default branch `main`.

(Ignore the commit that makes an update to the module .gitlab-ci.yml)

There is a commit that makes changes to the `firstproject` module, which results in the build job for the firstproject
module being run.

We also see in the [change-detection-firstproject-job](https://gitlab.com/personal6855003/conditionalcicd/-/jobs/5970397528)
that the script did in fact fetch the source branch `origin/chenenliu/change-firstproject-mr` and the target branch
`origin/main` for comparison.

![change-detection-firstproject-job log](assets/img_8.png)